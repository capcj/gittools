#!/usr/bin/perl -w
use strict; 
use warnings;
use Switch;
use JSON;
use Data::Dumper;
use constant HAS_GIT => `git --version 2>/dev/null`;
use constant HELP_MSG => "
  =================================\n
              GIT TOOLS\n
  =================================\n".
  "-i or --info [username]        Lists user info\n" .
 (HAS_GIT ? "-gar or --get-all-repos [username]:[password]        Download all the user repos\n" : "") . 
  "-sar or --show-all-repos [username]:[password]        Show all the user repos
-sur or --show-users-repos [username]        Show the user public repos
-wur or --wipe-user-repos [username]:[password]        Wipe the user repos [DANGEROUS!!!]
";
use constant MAIN_TOOL => "curl --silent ";
use constant API_URL => "https://api.github.com";

if (scalar(@ARGV) eq 0) {
  print HELP_MSG;
  exit;
}
sub printHash {
  my (%hash) = @_;
  foreach my $item(keys %hash) {
    if (!$hash{$item} eq "") {
      print ucfirst($item) . ": " . $hash{$item} . "\n";
    }
  }
}
sub printHashList {
  my $counter = 0;
  my @list = @_;
  for my $repo (@list) {
    if (ref($repo) eq '') {
      next;
    }
    $counter++;
    print "==================\n";
    print " REPO $counter \n";
    print "==================\n";
    printHash(%$repo);
    print "==================\n";
    print "==== END REPO ====\n";
    print "==================\n";
  }
  print "\n Total Repositories: $counter";
}
sub getUserRepos {
  my @credentials = split(/:/, $ARGV[1]);
  my $command = MAIN_TOOL . API_URL . "/user/repos -u $ARGV[1]";
  my $githubResponseStr = decode_json(`${command}`);
  my @githubResponse = @$githubResponseStr;
  return \@credentials, \@githubResponse;
}
sub getUsersRepos {
  my @credentials = split(/:/, $ARGV[1]);
  my $command = MAIN_TOOL . API_URL . "/users/$ARGV[1]/repos";
  my $githubResponseStr = decode_json(`${command}`);
  my @githubResponse = @$githubResponseStr;
  return \@credentials, \@githubResponse;
}
my $command = '';
switch ($ARGV[0]) {
  case ("-wur" || "--wipe-user-repos") {
    if (!$ARGV[1] eq "" && HAS_GIT) {
      my (@userReposInfo) = getUserRepos();
      if ($userReposInfo[1] == 0) {
        die "The user $userReposInfo[0][0] don't have repositories to get";
      }
      my @userRepos = @{$userReposInfo[1]};
      my $totalRepos = 0;
      for my $repo (@userRepos) {
        my %repoHashList = %$repo;
        system(MAIN_TOOL . ' -X DELETE ' . API_URL . "/repos/$userReposInfo[0][0]/$repoHashList{'name'} -u $ARGV[1]");
        $totalRepos++;
      }
      print "==================================\n";
      print "Wiped all the $totalRepos repositories!!! (6)\n";
      print "==================================\n";
    } else {
      print "Insert the USERNAME:PASSWORD after the getall command";
    }
  }
  case ("-gar" || "--get-all-repos") {
    if (!$ARGV[1] eq "" && HAS_GIT) {
      my (@userReposInfo) = getUserRepos();
      if ($userReposInfo[1] == 0) {
        die "The user $userReposInfo[0][0] don't have repositories to get";
      } else {
        system("mkdir -p $userReposInfo[0][0]/repositories");
      }
      my @userRepos = @{$userReposInfo[1]};
      print "\nGathered all the repos, if you want to skip one, press Ctrl^C to go to the next\n";
      for my $repo (@userRepos) {
        my %repoHashList = %$repo;
        if ($repoHashList{'clone_url'} ne "") {
          my $cloneUrl = $repoHashList{'clone_url'} =~ s/github/"$ARGV[1]\@github"/gr; 
          system("git clone $cloneUrl $userReposInfo[0][0]/repositories/$repoHashList{'name'}");
        } 
      }
      print "==================================\n";
      print "Downloaded all the repositories\n";
      print "==================================\n";
    } else {
      print "Insert the USERNAME:PASSWORD after the getall command";
    }
  }
  case ("-sar" || "--show-all-repos") {
    if (!$ARGV[1] eq "" && $ARGV[1] =~ /:/) {
      print "================================================\n";
      print "================ USER REPOSITORIES =============\n";
      print "================================================\n";
      my @userRepoInfo = @{(getUserRepos())[1]};
      printHashList(@userRepoInfo);
    } else {
      print "Insert the USERNAME:PASSWORD after the getall command";
    }
  }
  case ("-sur" || "--show-user-repos") {
    if (!$ARGV[1] eq "") {
      print "================================================\n";
      print "================ $ARGV[1] REPOSITORIES =============\n";
      print "================================================\n";
      my @userRepoInfo = @{(getUsersRepos())[1]};
      printHashList(@userRepoInfo);
    } else {
      print "Insert the USERNAME after the getall command";
    }
  }
  case ("-i" || "--info") {
    if (!$ARGV[1] eq "") {
      $command = MAIN_TOOL . API_URL . "/users/$ARGV[1]";
      my $githubResponseStr = decode_json(`${command}`);
      my %githubResponse = %$githubResponseStr;
      print "================================================\n";
      print "=================== USER INFO ==================\n";
      print "================================================\n";
      printHash(%githubResponse);
      exit;
    } else {
      print "Insert the USERNAME after the getall command";
    }
  }
  else {
    print HELP_MSG;
  }
}
