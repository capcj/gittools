# Git Tools
A small Perl script designed to make my life easier with Github API

## Usage
```bash
perl gittools.pl
```

## Requirements
- Must have cURL and git (for some features) installed in your system;
- Install JSON, Switch modules with CPAN;
